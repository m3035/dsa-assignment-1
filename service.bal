import ballerina/http;
import ballerina/io;


# update learners profiles(JSON)
#
      
# + lastname - Field Description      
# + username - Field Description      
# + subject - Field Description      
# + score - Field Description
type  Learner record{#["name":Sam, "lastname": Hans, "username":SH, "subject":math, "score":B]
string name;
string lastname;
string username;
string subject;
string score;
};

Learner [] learnersList =[];


#@openapi:ServiceInfo{
 #  contract: “/path/to/openapi.json|yaml”,
 #[ tag : “store” ],
 #[ operations: [“op1”, “op2”] ] 
  # [ failOnErrors]: true/false → default : true
   #[ excludeTags ]: [“pets”, “user”]
   # [ excludeOperations: [“op1”, “op2”] ]

openapi:3.0.0
info:
titile: Learner API
description: Learners profiles service
contact:
name: LearnersProfile
url: http://localhost:9098/LearnersProfile
version:1.0.0
server:
url:http://localhost:9098/LearnersProfile
path:
/Learner:
description: Learner resources are writeen here
get: 
description:Operation to fetch learner
parameters:
name: username
required: true
schema:
type: string
example: SamJ
# Description  
 service/LearnersProfile on new http:Listener(9098) {

 # Description
 #
 # + new_item - Parameter Description
 # + return - Return Value Description  
 # Description
 #

resource function post addLearner(@http:Payload Learner new_item) returns json {
    io:println("Inserting new learner");
    any res =learnersList.push(new_item);
    return{"Add":"Successfully added"+"new_item.name"};

}

resource function get getLearner() returns Learner[] {
    io:println("Producing the learners list to client");
    return learnersList;
    
}

resource function get all() returns Learner[] {
io:println("handling GET request to /Learner/all");
return learnersList;
}
resource function post insert(@http:Payload Learner new_user) returns json {
io:println("handling POST request to /users/insert");
learnersList.push(new_user);
return {done: "Ok"};
}

    # A resource respresenting an invokable API method
    # accessible at `/hello/sayHello`.
    #
    # + caller - the client invoking this resource
    # + request - the inbound request
    resource function get sayHello(http:Caller caller, http:Request request) {

        // Send a response back to the caller.
        error? result = caller->respond("Hello Ballerina!");
        if (result is error) {
            io:println("Error in responding: ", result);
        }
    }
}
