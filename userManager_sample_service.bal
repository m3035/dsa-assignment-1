import ballerina/grpc;
import ballerina/log;

listener grpc:Listener ep = new (9090);
User [] FuncList = [];
@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "userManager" on ep {

    remote function greetUser(User value) returns response|error {
        log:printInfo(value.name);
        response res = {msg:"Hello "+value.name,currentUser:value};
        return res;
    }
    remote function add_new_fn(User value) returns response|error {
        FuncList.push(value);
        io:println("Function created.");
        return "Function successfully added :"+(value.user_id);
    
    }

    remote function add_fns(CreateRequest value) returns response|error {
        FuncList.push(value);
        return "Function successfully added :"+(value.user_id).toString();
    
    }

remote function delete_fn(Delete_fn value) returns response|error {
        FuncList.push(value);
        return "Function successfully added :"+(value.user_id).toString();
    
    }

}
